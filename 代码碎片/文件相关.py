import os,sys,shutil


'''  将当前工作目录 从 解释器所在目录 切换到 脚本所在目录(sublime不需要这个,vsc，pycharm需要)  '''
'''这个代码有问题，在spyder下'''
# print(os.getcwd())  				 # 执行脚本解释器所在目录|当前工作目录
os.chdir(sys.path[0])				 # 切换工作目录 到 脚本所在目录




'''  经典操作  '''

# 取运行目录
# 获取当前运行脚本路径
sys.argv[0]						# 获取主执行文件路径(包括文件名), 可能是一个相对路径，通常abspath是保险.
sys.path[0]						# 获取主执行文件路径(只有路径), 可能是一个相对路径，通常abspath是保险.
os.getcwd().replace('\\', '/')	# 获取解释器执行脚本时候的所在地方
os.path.exists(path)								# 文件或目录是否存在
os.startfile('D:/jpg')								# 用系统默认应用打开文件
os.system('start "" /d "应用路径" /wait "应用.exe" "文件绝对路径"') # 用指定应用打开文件
os.path.split('C:/soft/python/test.py')				# 分割绝对路径（目录/文件名）	# ('C:/soft/python', 'test.py')
os.path.splitext('test.py')							# 分割文件名（主文件名.扩展名）	# ('test', '.py')
os.path.getsize(file)								# 文件大小
import codecs
with codecs.open(path,'w','utf-8') as f:x=f.read()	# 指定编码读写文件

os.path.getctime(path)  # 获取文件的创建时间
os.path.getmtime(path)  # 获取文件的修改时间

'''  目录操作  '''

os.path.exists('D:/jpg')	# False					# 目录或文件是否存在

if not os.path.exists(path):						# 创建目录
	os.makedirs(path)

import shutil
file = 'E:/jpg'
newpath = 'D:/'										# 移动文件到指定文件夹
shutil.move(file,(newpath.replace('\\','/')+'/'+os.path.basename(file)).replace('//','/'))


def WinFileNameFormat(filename):
    '''文件名不能包含\/:*?"<>|'''
    filename = re.sub(r'[\/:*?"<>|]', '_', filename)  # 剔除掉文件名中的特殊字符
    return filename


'''  删除操作  '''

import shutil
shutil.rmtree(path)									# 删除文件夹及其文件

def file_delete(files):								# 删除文件
	# 判断路径是否存在，避免删除出错
	if os.path.exists(file):
		# 判断是否是文件，避免删错整个目录删除
		if os.path.isfile(file):
			os.remove(file)


'''  文件操作  '''

with open('file', 'rb') as f:
	f.seek(0, 0)		# 指针移动到文件起始位置
	print(f.tell())		# 打印当前指针位置
	x = f.read(500)  	# 读入文件开头500个字节
	print(x)
	f.seek(-500, 2)  	# 指针移动到文件倒数500的位置
	print(f.tell())  	# 打印当前指针位置
	x = f.read(500)  	# 读入文件结尾500个字节
	print(x)


def FileSizeFormat(fileSize):						# 文件大小（转换为'MB','GB'…形式）
	'''
	将计算机标注的文件大小'Bytes'，
	转换成可识别的'KB','MB','GB','TB','PB'形式
	fileSizeFormat(1048561622)
	>>> 999.99 MB
	fileSizeFormat(1048571622)
	>>> 0.98 GB
	'''
	if fileSize >= 1: # 避免 filseSize == 0 时报错
		# 开始转换
		for count in ['Bytes','KB','MB','GB','TB','PB']:
			if fileSize >= 0.97655 and fileSize < 1024.0:
				readableSize = "%3.2f %s" % (fileSize, count)
			fileSize /= 1024.0
	else:
		readableSize = "%d %s" % (fileSize, 'Bytes')
	return readableSize


def fileSearch(path='.', repat= r'.*', ignorecase=True, fullpath=True, deep=True):
    """
    文件查找：
        文件夹及子文件夹下，所有匹配文件，返回list文件列表，绝对路径形式
    Args:
        path: 文件路径（默认当前路径）
        repat: 文件名正则匹配，不区分大小写（默认匹配所有文件）比如：r'\d+\.jpg'，匹配所有数字名称的.jpg文件
        ignorecase: True忽略大小写，否则区分大小写
        fullpath: True返回文件的绝对路径，否则只返回文件名
        deep: True获取子目录下的文件，否则忽略子目录下的文件
    Returns:
        files_match: 文件列表
    """
    # 获取文件夹，及子文件夹下所有文件，并转为绝对路径
    files = []
    repat = '^'+repat+'$'
    oswalk = os.walk(path)
    # walk结果形式 [(folder:文件夹,[childrenFolderList:该文件夹下的子文件夹],[fileList:该文件夹下的文件]),(子文件夹1,[子子文件夹],[]),(子文件夹2,[],[])...]
    # 该遍历会走遍所有子文件夹，返回上述形式的结果信息。
    oswalk = oswalk if deep else [oswalk[0]]  # 是否遍历子目录
    ignorecase = re.IGNORECASE if ignorecase else 0 # 是否忽略大小写
    # 找出符合条件的文件名，合并绝对路径后，加入到返回列表
    for folder, childrenFolderList, fileList in oswalk:
        for file in fileList:
            # 文件名是否符合要求
            if re.match(pattern=repat, string=file, flags=ignorecase):
                # 返回绝对路径或只是文件名
                filepath = os.path.abspath(os.path.join(folder, file)).replace(
                    '\\', '/') if fullpath else file
                files.append(filepath)
    print('找到{0}个文件'.format(len(files)))
    # 返回满足要求的
    return files




def OsWalk(path):
	''' 
	获取文件夹，及子文件夹下所有文件，并转为绝对路径
	Args:
		path: 文件夹路径
	Returns:
		files: 所有文件（绝对路径）
		dirs: 所有目录（当前目录及所有子目录）
	'''
	files, dirs = [], []  			# 初始化返回数组
	# record 格式 [(walkpath,[dirlist],[filelist]),...]
	for record in os.walk(path):
		dirname = record[0].replace('\\', '/')
		dirs.append(dirname)		# 加入目录
		for filename in record[2]:  # 加入文件（绝对路径形式）
			filepath = os.path.join(dirname, filename)
            filepath = os.path.abspath(filepath).replace('\\', '/')
            files.append(filepath)
	return files, dirs



def FileMd5(filename): # 计算md5
	"""
	用于获取文件的md5值
	:param filename: 文件名
	:return: MD5码
	"""
	import hashlib
	if not os.path.isfile(filename):  # 如果校验md5的文件不是文件，返回空
		return
	myhash = hashlib.md5()
	f = open(filename, 'rb')
	while True:
		b = f.read(8096)
		if not b:
			break
		myhash.update(b)   
	f.close()
	return myhash.hexdigest()


def FastMd5(file_path,split_piece=256,get_front_bytes=8):
    """
    快速计算一个用于区分文件的md5（非全文件计算，是将文件分成s段后，取每段前d字节，合并后计算md5，以加快计算速度）

    Args:
        file_path: 文件路径
        split_piece: 分割块数
        get_front_bytes: 每块取前多少字节
    """
    size = os.path.getsize(file_path) # 取文件大小
    block = size//split_piece # 每块大小 
    h = hashlib.md5()
    # 计算md5
    if size < split_piece*get_front_bytes: 
        # 小于能分割提取大小的直接计算整个文件md5
        with open(file_path, 'rb') as f:
            h.update(f.read())
    else:
        # 否则分割计算
        with open(file_path, 'rb') as f:
            index = 0
            for i in range(split_piece):
                f.seek(index)
                h.update(f.read(get_front_bytes))
                index+=block
    return h.hexdigest()


def FindDuplicateFile(fp_arr):
    """
    快速查找大小相同、md5一致的重复文件

    Args:
        fp_arr:文件列表
    """
    # 将文件大小和路径整理到字典中
    d = {}  # 临时词典 {文件大小1:[文件路径1,文件路径2,……], 文件大小2:[文件路径1,文件路径2,……], ……}
    for fp in fp_arr:
        size = os.path.getsize(fp)
        d[size]=d.get(size,[])+[fp]
    # 列出相同大小的文件列表
    l = [] # 临时列表 [[文件路径1,文件路径2,……], [文件路径1,文件路径2,……], ……]
    for k in d:
        if len(d[k])>1:
            l.append(d[k])
    # 核对大小一致的文件，md5是否相同
    ll = [] # 临时列表 [[文件路径1,文件路径2,……], [文件路径1,文件路径2,……], ……]
    for f_arr in l:
        d = {} # 临时词典 {文件大小1:[文件路径1,文件路径2,……], 文件大小2:[文件路径1,文件路径2,……], ……}
        for f in f_arr:
            fmd5 = fastmd5(f)
            d[fmd5]=d.get(fmd5,[])+[f]
        # 找到相同md5的文件
        for k in d: # 相同大小的文件，核对一下md5是否一致
            if len(d[k])>1:
                ll.append(d[k])
    print('查重完毕，发现{0}处重复'.format(len(ll)))
    for i in ll:
        print(i)
    return ll



def FileCopyOrMoveAutoRename(filePath,toFolder,plan='COPY'):
    '''
    将文件拷贝或移动到指定路径
    如果有同名文件，则以_1，_2的形式保存，以此类推
    并将保存名返回。
    Args:
        filePath: 文件路径
        toFolder: 要移动到的文件夹
        plan: 复制('COPY')或剪切('MOVE')方式移动，默认复制
    Returns:

    '''
    # 整理路径为'/'方式且以'/'结尾
    toFolder = toFolder.replace('\\','/')
    if toFolder[-1]!='/':toFolder+='/'
    # 要移动文件，取出文件名部分，并分割为名称和扩展名
    fileName = os.path.split(filePath)[1]             # 只取文件名部分
    fileNameL,fileNameR = os.path.splitext(fileName)  # 文件名分割成 名称 和 扩展名
    # 查看要移动的位置是否存在同名文件，如有则按照_1，_2的形式保存，以此类推
    index = 0 
    while True:
        if index==0:
            filePathNew = toFolder+fileName
        else:
            filePathNew = toFolder+fileNameL+'_'+str(index)+fileNameR
        if not os.path.exists(filePathNew):
            break
        else:
            index+=1
    # 复制或剪切文件
    if plan=='COPY':
        shutil.copy(filePath,filePathNew)
    elif plan=='MOVE':
        shutil.move(filePath,filePathNew)
    else:
        print('Error Plan')
        raise
    # 返回结果
    return filePathNew


def BuildMutiFolder(path,names):
    """
    在一个文件夹下，创建多个文件夹，如果文件夹已经存在，检查该文件夹下是否已经存在文件，
    Args:
        path: 路径
        names: 要创建的文件夹名，list格式
    Returns:
        pathnames: 创建好的文件夹名
    """
    path = path.replace('\\','/')
    if path[-1]!='/':
        path+='/'
    pathnames = [path+i+'/' for i in names]
    for i in pathnames:
        if not os.path.exists(i):
            os.mkdir(i)
        else:
            iNum = len(fileSearch(i))
            if iNum>0:
                print('Warning: The Folder{0} Still Have {1} Files In There, Remove Them, Then Try Again.'.format(i,iNum))
                raise
    return pathnames
