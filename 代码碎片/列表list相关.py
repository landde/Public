

'''列表相关，set方式处理'''

arr = [6,23,2,5,64,6]
a = [1,2,3]
b = [2,3,4]
b[::-1]						# [4, 3, 2]				# 倒序
list(set(arr))				# [64, 2, 5, 6, 23]		# 列表去重（未知顺序）
list(set(a)-set(b))			# [1]					# 列表差集，相对补集，删除已有
list(set(a)|set(b))			# [1, 2, 3, 4]			# 列表并集
list(set(a)&set(b))			# [2, 3]				# 列表交集
a.remove('')										# 列表删除空项


def list_find_index(arr,str_pat):					# 查找某条记录位置(返回位置list)
	index = []
	for i, v in enumerate(arr):
		if v==str_pat:
			index.append(i)
	return index
