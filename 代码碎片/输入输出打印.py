
# 打印不换行
print('a',end=' ',flush=True)		# (python 3.x 的方法)
# print 'a',			# (python 2.x 的方法)
# sys.stdout.flush()

def muti_input(showwords='', times=2):
    '''
    令input支持多行，连续多次出现换行符则终止。
    '''
    text = ''
    print(showwords)
    br = 0  # 换行符出现次数

    while True:  # 无限次input输入
        input_ch = input()
        if input_ch != '':
            br = 0
            text += input_ch + '\n'
        elif br < times:  # 连续多次出现换行符
            br += 1
        else:    # 多个换行符，终止输入
            break

    return text


import sys
import time
import msvcrt # 只能windows上用


def readInput_TimeLimit(caption, default='', timeout=3):
    """
    windows下使用，等待输入，超时自动停止
    """
    start_time = time.time()
    sys.stdout.write('%s(%d秒自动跳过):' % (caption, timeout))
    sys.stdout.flush()
    input = ''
    while True:
        ini = msvcrt.kbhit()
        try:
            if ini:
                chr = msvcrt.getche()
                if ord(chr) == 13:  # enter_key
                    break
                elif ord(chr) >= 32:
                    input += chr.decode()
        except Exception as e:
            pass
        if len(input) == 0 and time.time() - start_time > timeout:
            break
    print('')  # needed to move to next line
    if len(input) > 0:
        return input+''
    else:
        return default


def getpass(prompt="Password: "):
    """
    Linux下实现，password不可见方法的实现
    """
    import termios
    import sys
    fd = sys.stdin.fileno()
    old = termios.tcgetattr(fd)
    new = termios.tcgetattr(fd)
    new[3] = new[3] & ~termios.ECHO          # lflags
    try:
        termios.tcsetattr(fd, termios.TCSADRAIN, new)
        passwd = input(prompt)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old)
    return passwd



# 当前根据平台不同，加载不同模块
import platform
systemtype = platform.platform()[:3].lower()  # lin/win系统
if systemtype == 'win':
    import msvcrt  # Windows下python自带
elif systemtype == 'lin':
    import termios  # Linux下python自带
    import tty  # Linux下python自带
else:
    print('Unknown platform:', platform.platform())
    raise


def input_LengthLimit(prompt='', length=1):
    """
    Linux/Windows下实现，input，到达指定长度自动停止，这个可能会影响一些情况下Ctrl+Z停止程序，不过当前我用功能里面够了，其他地方用的时候注意。
    """
    sys.stdout.write(prompt)
    sys.stdout.flush()
    input = ''
    if platform.platform()[:3].lower() == 'win':
        # windows
        while True:
            ini = msvcrt.kbhit()
            try:
                if ini:
                    chr = msvcrt.getche()
                    if ord(chr) == 13:  # enter_key
                        break
                    elif ord(chr) >= 32:
                        input += chr.decode()
            except Exception as e:
                pass
            if len(input) == 1:
                break
    elif platform.platform()[:3].lower() == 'lin':
        # linux
        while True:
            fd = sys.stdin.fileno()
            old_settings = termios.tcgetattr(fd)
            try:
                tty.setraw(sys.stdin.fileno())
                ch = sys.stdin.read(1)
            finally:
                termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
            if ch == '\r' or ch == '\n':
                break
            elif ch == "\b" or ord(ch) == 127:
                if len(input) > 0:
                    sys.stdout.write("\b \b")
                    input = input[:-1]
            else:
                input += ch
                if len(input) == length:
                    break
    else:
        print('Unknown platform:', platform.platform())
        raise
    print('')  # needed to move to next line
    return input


aaa = input_LengthLimit(prompt='', length=3)
print(aaa)
