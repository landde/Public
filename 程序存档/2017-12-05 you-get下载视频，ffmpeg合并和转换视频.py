#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Created on 2017-12-5
@author: watfe
@title: 使用you-get下载视频，使用ffmpeg合并和转换视频

其他没啥说的，会用的自然会用。
'''


import os
import you_get		# pip install you-get
from subprocess import call
import re


# 粘贴视频url
urls= '''

https://www.bilibili.com/video/av9639021?from=search&seid=9489379576990531368

'''.srtip().split('\n')




def main():
	video_dowload()
	# mp4join() 					# 手动合并（因为有的时候you-get合并可能出错）
	# os.system("shutdown -s -t 0")	# 下载完成自动关闭电脑


def cmd():
	# cmd方式使用指令
	'''
	# 命令行中直接用ffmpeg进行视频转换或压缩。
	1、安装ffmpeg
	2、进入命令行，cmd
	3、转换到h264MP4格式还加了一些压缩参数 ffmpeg -i zhaobangall.mp4 -b:a 32k -s 320x640 -r 8 out.mp4
	'''

	'''
	# 命令行中直接用使用you-get包下载各种视频
	1、安装you-get。
		https://you-get.org/#legal-issues
	2、分析视频的信息，然后将可下载的视频按照清晰度列出来。
		you-get -i 'http://v.youku.com/v_show/id_XMjY3NzY3NTMyNA==.html?spm=a2h0z.8244218.2371631.3&from=y1.9-3.1'
	3、例如我要下载最清晰的版本，就输入超清那里提示的命令。
		you-get -F flvhd 'http://v.youku.com/v_show/id_XMjY3NzY3NTMyNA==.html?spm=a2h0z.8244218.2371631.3&from=y1.9-3.1'
	'''

def video_dowload():
	# 下载视频

	'''视频下载不了的时候
	1、可能需要更新一下you-get。
		pip install you_get -U  
	2、有的时候版本还没更新，但是github上已经更改了部分内容。
		直接下载 https://github.com/soimort/you-get > 选Clone or download > Download ZIP 。
		用压缩包中的you_get文件夹，覆盖掉原本 C:/Users/Name/AppData/Local/Programs/Python/Python36-32/Lib/site-packages/you_get 里面原本的内容	
		'''
	for i in range(len(urls)):
		print("Start downloading...\n%d\n" % i)
		call("you-get -d " + urls[i], shell=True)
		#call("you-get -i " + urls[i], shell=True)
		#call("you-get -F flv " + urls[i], shell=True)
		#call("you-get -d -u " + urls[i] + " -y 127.0.0.1:1080", shell=True) # 通过代理1080端口下载


def mp4join():
	# 合并mp4文件
	'''
	安装ffmpeg
	1、下载FFmpeg http://ffmpeg.zeranoe.com/builds/ 
	2、解压到‘C:\ffmpeg’ 
	3、配置环境变量‘;C:\ffmpeg\bin;’ 
	4、输入命令‘ffmpeg –version’测试
	'''
	import os,shutil,time
	pwd = os.getcwd().replace('\\','/')
	os.mkdir(pwd+'/join')
	L=[]
	# 当前目录下所有mp4文件
	tail = '.mp4'
	for root, dirs, files in os.walk(os.getcwd()):
		del root, dirs	# 避免pylint弹出变量未使用警告
		for file in files:  
			if os.path.splitext(file)[1] == tail:
				L.append(file)
	# 分组
	G = [f[:-4-len(tail)] for f in L]	# 去除[00].mp4部分
	G = list(set(G))					# 文件名去重
	G = [[f] for f in G]				# 变成这个格式[[],[],[]……]
	for f in L:							# 将文件名加入到对应的列表中
		for i in range(len(G)):
			if G[i][0]==f[:-4-len(tail)]:
				G[i].append(f)
	for i in range(len(G)):
		print('"%s" %s个文件'%(G[i][0],len(G[i])-1))
	print('5秒后开始合并：……')
	time.sleep(5)
	# 开始合并
	for i in range(len(G)):
		name = G[i][0]			# 该视频名称
		files = G[i][1:]		# 该视频所有文件碎片名称
		os.mkdir(pwd+'/temp') 	# 创建临时文件夹
		filelist = ''			
		for f in files:			# 重命名到临时文件夹中（因为文件名中包含中文或[]，合并时会报错，所以需要先都重命名为数字）
			newname = f[len(name)+1:-len(tail)-1]+tail			# 00.mp4
			filelist+="file '"+newname+"'\n" 					# 构造filelist.txt
			os.rename(pwd+'/'+f,pwd+'/temp/'+newname)			# 文件碎片重命名
		with open('filelist.txt','w') as f:f.write(filelist)	# 保存filelist.txt
		call('ffmpeg -f concat -i filelist.txt -c copy output.mp4', shell=True) # 执行合并
		os.rename(pwd+'/output.mp4',pwd+'/join/'+name+tail)		# 重命名输出视频名称
		shutil.rmtree(pwd+'/temp') 								# 删除临时文件夹


if __name__ == '__main__':
	main()
