#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# pip install selenium
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import random,time,re
import urllib.request

'''
Created on 2018-1-13
@author: watfe
@title: selenium及phantomjs无界面浏览器的使用：根据韩剧名称取豆瓣评分介绍

用豆瓣进行电影搜索的时候，搜索结果看不出来是怎么回传的数据。
不得只能通过phantomjs加载后，提取相关节点。
'''


# 将当前工作目录 从 解释器所在目录 切换到 脚本所在目录(sublime不需要这个,vsc，pycharm需要)
import os
import sys
cwd = sys.path[0]  					 # 被初始执行脚本所在目录
if os.path.isfile(cwd):  			 # py2exe打包后，路径变成 cwd+\library.zip
    cwd, filen = os.path.split(cwd)  # 处理一下，只提取目录名部分
cwd = cwd.replace('\\', '/')
if cwd[-1] != '/':cwd += '/'
os.chdir(cwd)						 # 切换工作目录 到 脚本所在目录



aaa='''延南洞539
操心
花游记
坏家伙们2恶之都市
只是相爱的关系
世上最美丽的离别
黑骑士
不是机器人啊
Jugglers
无理的英爱小姐16季
Two Cops
疑问的一胜
Untouchable
机智的监狱生活
鱼死网破 / 理判事判
金钱之花 / 钱花
Melo Holic
Bravo My Life
Black
卞赫的爱情
The Package
告白夫妇 / Go Back夫妇
付岩洞复仇者们
Mad Dog
魔女的法庭
20世纪少男少女
今生是第一次
当你沉睡时
Andante
爱情的温度
时尚妈妈
内衣少女时代
Argon
我黄金光辉的人生
准备饭桌的男人
医疗船
青春时代2
名不虚传
Manhole / 奉必梦游仙境
救救我
最强送餐员
犯罪心理
操作/ 操控
死而复生的男人
再次相遇的世界
学校2017
王在相爱
有品位的她
河伯的新娘
秘密森林
Duel / 克隆人
最佳的一击
七日的王妃
我的野蛮女友
相连的两个世界
三流之路
守望者
小偷先生
君主-假面的主人
奇怪的搭档
个人主义者智英小姐
Man X Man
焦急的罗曼史
姐姐风采依旧
芝加哥打字机
推理的女王
耳语/悄悄话
隧道
她爱上了我的谎
自体发光办公室'''.split('\n')



# PTJSbrowser构造好的类，可直接调用
class PTJSbrowser:
    def __init__(self):
        # 头部代理
        UserAgents = [
            "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36"
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36",
            "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET CLR 2.0.50727; Media Center PC 6.0)",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11",
            "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"
        ]
        # 引入配置对象DesiredCapabilities
        dcap = dict(DesiredCapabilities.PHANTOMJS)
        service_args=[]
        useragent = random.choice(UserAgents)
        dcap["phantomjs.page.settings.userAgent"] = useragent            #从USER_AGENTS列表中随机选一个浏览器头，伪装浏览器
        dcap["phantomjs.page.settings.loadImages"] = False               #不载入图片，爬页面速度会快很多
        #service_args = ['--proxy=127.0.0.1:9999','--proxy-type=socks5'] #设置代理
        service_args.append('--load-images=false')                       #关闭图片加载
        service_args.append('--disk-cache=yes')                          #开启缓存
        service_args.append('--ignore-ssl-errors=true')                  #忽略https错误   
        # 初始化
        self.__browser = webdriver.PhantomJS(executable_path='phantomjs.exe',desired_capabilities=dcap,service_args=service_args)   # 从 http://phantomjs.org/download.html 下载压缩包，将 phantomjs.exe 移动到“C:\Users\waterd\AppData\Local\Programs\Python\Python35-32\Scripts\”中
        self.__browser.implicitly_wait(3)           # 隐式等待5秒，可以自己调节
        self.__browser.set_page_load_timeout(30)    # 以前遇到过driver.get(url)一直不返回，但也不报错的问题，这时程序会卡住，设置超时选项能解决这个问题。 类似于requests.get()的timeout选项，driver.get()没有timeout选项
        self.__browser.set_script_timeout(30)       # 设置10秒脚本超时时间

    def __del__(self):
        # 构造析构函数
        self.__browser.quit()

    def get(self,url,trytimes=3,waitsecond=3):
        """
        获取网页
        url: 网址
        trytimes: 最大重试次数
        waitsecond: 加载等待时长
        :return: selenium结构
        """
        import time
        webS = None
        for i in range(trytimes):
            try:
                self.__browser.get(url)  # 加载目录页
                time.sleep(waitsecond)   # 等待加载完毕
                self.__browser.get_screenshot_as_file('show.png') # 保存截图，异常的时候可以看看截图上的图像对不对
                webS = self.__browser
                break
            except:
                pass
        return webS

'''    ***************************************以下是代码正文***************************************    '''

def main():
    # 创建webdriver PhantomJS对象
    browser = PTJSbrowser()
    for title in aaa:
        # 打开豆瓣搜索韩剧
        url = 'http://movie.douban.com/subject_search?search_text='+urllib.request.quote(title)+'&cat=1002'
        webS = browser.get(url)
        if webS != None:
            # 能确定的父节点是root，剩下的子节点都只有class且是随机值，所以x是取root的子div的第二个子div的子div
            x = 'div[id="root"]>div div:nth-child(2)>div'       # 构造by_css_selector
            div = webS.find_element_by_css_selector(x)          # 查找元素
            text = div.get_attribute('outerHTML')               # 取内容
            print(text)
        
        print('<hr/>')

if __name__ == '__main__':
    main()


