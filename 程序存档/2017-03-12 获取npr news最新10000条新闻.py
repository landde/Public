#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from lxml import etree
import re
import requests
import os
import time
import fnmatch
import multiprocessing
import codecs
from html.parser import HTMLParser
import shutil

'''
Created on 2017-3-12
@author: watfe
@title: 获取npr news最新10000条新闻

npr新闻 news 下属
http://www.npr.org/sections/news/archive?start=1 
到 
http://www.npr.org/sections/news/archive?start=10000 
也就是最新的10000条news（上次获取的得到的是2017年2月27日到2016年1月8日的）
下载并提取正文部分保存到文本文件中。

依稀记得当时弄英语单词学习的程序，想要统计词频，以及已掌握的单词占其单词量来着。
然后就想抓取npr的新闻作为词频分析依据。

从备注看，貌似当初是还不是想写个一次性的，而是想写个能通用的东西。
现在没时间弄，不细看了，也不知道还能不能跑通。
'''

def files_search(path, match=''):  # 查找文件*.*
	"""
	遍历查找path文件夹下所有文件（包含子文件夹），返回所有匹配match的文件（match为空返回所有文件）
	fnmatch 模块使用模式来匹配文件名.
	模式语法和 Unix shell 中所使用的相同. 星号(*) 匹配零个或更多个字符, 问号(?) 匹配单个字符.
	你也可以使用方括号来指定字符范围, 例如 [0-9] 代表一个数字. 其他所有字符都匹配它们本身.
	Args:
		path: 文件路径
		match: 匹配文件名
		return: 文件绝对路径数组
	Returns:
		文件列表
	Raises:
		IOError:
	"""
	files = []  # 初始化返回数组
	# 遍历文件夹获取所有文件到files列表
	for record in os.walk(path):
		# walk结果形式 [(walkpath,[dirlist],[filelist]),...]
		dirname = record[0].replace('\\', '/')
		for filename in record[2]:
			files.append(os.path.join(dirname+'/'+filename))
	# 匹配文件名
	if match == '':                                 # 不需要匹配，返回全部文件
		files_fnmatch = files
	else:                                           # 需要匹配
		files_fnmatch = []
		for file in files:
			basename = os.path.basename(file)      # 取文件名
			if fnmatch.fnmatch(basename, match):    # 匹配测试
				files_fnmatch.append(file)
	return files_fnmatch


def requests_download(url, path, proxyip=''):  # 下载文件
	'''
	下载图片或网页等小文件
	Args:
		url: 下载地址
		path: 保存文件
	'''
	proxies = {} if proxyip == '' else {'http': 'http://'+proxyip}  # 初始化proxies
	file_content = requests.get(url, proxies=proxies).content
	with open(path, 'wb') as f:
		f.write(file_content)
	time.sleep(0.01)  # 避免多线程时下载过于频繁

def requests_download_muti(urls,names,pip=4,proxyip=''): # 多进程批量下载文件
	'''
	multiprocessing.Pool 多进程下载图片或网页等小文件
	Args:
		urls: 下载地址列表
		names: 文件保存路径列表
		pip: 并发进程数量（和cpu核心数有关，4核cpu最大为4）
	'''
	print("mutiDownload start")
	pool = multiprocessing.Pool(processes=pip)     # processes=4是最多并发进程数量。
	for i in range(len(urls)):
		url = urls[i]
		name = names[i]
		pool.apply_async(requests_download, args=(url,name,proxyip)) # 注意要用apply_async，如果落下async，就变成阻塞版本了
	pool.close()
	pool.join()
	print("mutiDownload finished.")


def file_open(path,rwb='rb',text='',charset='utf-8'): # 文件读写
	"""
	文件读写，字符串或二进制形式，支持utf-8等编码读写
	Args:
		path: 文件路径
		rwb: 读或写，可以是r、rb、w、wb、a、ab中任意一种
		text: 要写入或返回的字符串或二进制编码
		charset: 用codecs提供的open方法来指定打开的文件的语言编码，它会在读取的时候自动转换为内部unicode
		return: 返回text
	Returns:
		返回读入文本
	"""
	if ('r' in rwb and (('a' in rwb) or ('w' in rwb))) or ('r' not in rwb and 'a' not in rwb and 'w' not in rwb):
		# 同时存在'ra'或'rw'，或'rwa'都不存在
		print('openfile rwb type error')
	elif 'b' in rwb:
		with open(path,rwb) as f:
			text = f.read() if 'r' in rwb else f.write(text)
	else:
		with codecs.open(path,rwb,charset) as f: # charset='utf-8' 之类的
			text = f.read() if 'r' in rwb else f.write(text)
	# return 返回
	return text


def html_getxpathlist(html,xpath_query):
	'''
	建议chrome浏览器安装XPath Helper Wizard，然后打开页面直接Ctrl-Shift-X，测试xpath
	'''
	root = etree.HTML(html)
	elms = root.xpath(xpath_query)
	result = []
	if len(elms)>0:
		for elm in elms:# <class 'lxml.etree._Element'>
			if etree.iselement(elm):
				elm_b = etree.tostring(elm)                 # 输出内容 <class bytes>
				elm_t = str(elm_b,encoding='utf-8')         # 转字符串 <class string>  （如果不加 encoding='utf-8' 转化后的字符串收尾会多出[b']和[']这三个字符）
			else:
				elm_t = str(elm)
			# etree 会自动处理<div ***></div>为<div ***/>的形式，这里将这种精简写法改回未处理之前的状态，避免浏览器显示出现异常
			while '/>' in elm_t:
				xiegang = elm_t.find('/>')
				leftkuohao = elm_t.rfind('<',0,xiegang)
				if leftkuohao>0 and xiegang>0:
					biaoqian = re.findall(r"(<\w+)", elm_t[leftkuohao:xiegang])[0]
					if biaoqian == '<br':# 避免<br/>的问题
						elm_t.replace('/>','>',1)
					else:
						elm_t.replace('/>','</'+biaoqian[1:]+'>',1)
				else: # 前面找不到'<'的异常点，也替换掉
					elm_t.replace('/>','>',1)
			# 处理html实体（etree会智能补全和加工html代码，导致HTML中的预留字符被替换为字符实体[&#****;]的样子，需要将其转化回正常字符）
			elm_s = html_unescapeEntities(elm_t) 
			result.append(elm_s)
	return result


def html_unescapeEntities(html): # 处理转义字符
	# 处理html中的字符实体(Entities)，有的时候像&amp;omega;需要处理多次才行
	html_parser = HTMLParser()
	oldlen = len(html)
	while 1:
		html = html_parser.unescape(html)
		newlen = len(html)
		if oldlen == newlen:
			break
		else:
			oldlen = newlen
	return html


def files_delete(files): # 删除一个或一组文件(只删文件不删目录)
	if isinstance(files,str):         # 判断变量是否是string，如果是则转化为list
		files=[files]
	for file in files:                # list中的文件逐个处理
		if os.path.exists(file):      # 判断路径是否存在，避免删除出错
			if os.path.isfile(file):  # 判断是否是文件，避免删错整个目录删除
				os.remove(file)       # 删除文件


def files_move(files,newpath): # 批量移动文件到某文件夹
	if newpath[-1:]=='/':       # 如果路径末尾是/，则删除该符号
		newpath = newpath[:-1]
	if isinstance(files,str):   # 判断是一个还是多个文件
		files=[files]
	for file in files:          # 移动文件
		shutil.move(file,newpath+'/'+os.path.basename(file)) # 移动文件


def list_delsame(arr):   # 去重
	"""
	:arr: 数组变量
	:return: 结果列表[,,]
	"""
	arr_return = []
	for record in arr:
		if record not in arr_return:
			arr_return.append(record)
	return arr_return


def creatlistpageurls():
	# 目录页urls
	urls = [] 
	x = range(1,10000,15)
	for i in x:
		urls.append('http://www.npr.org/sections/news/archive?start='+str(i))
	return urls

def downloadlistpages(urls):
	# 下载所有目录页
	paths = []
	for i in range(len(urls)):
		paths.append('./temp/%06d'% (i+1))
	requests_download_muti(urls,paths)


def xpathpagesurls():
	# 从目录页逐页提取正文页urls
	files = files_search('./temp/')
	totalurls = []
	for file in files:
		html = file_open(file,rwb='r')
		arr = re.findall(r'''class="item-info">\n\s+<h3 class="slug"><[^>]+>([^\<]+)</a></h3>[^\<]+<h2 class="title"><a href="([^\"]+)"[^>]+>([^<]+)</a></h2>''', html)
		print(len(arr))
		break
	
	# 	urls = html_getxpathlist(html,xpathSTMT)
	# 	if len(urls)==getnum or file==files[-1]:
	# 		totalurls+=urls
	# 		# 确认最后一个文件也成功提取了，则删除所有目录页
	# 		if file==files[-1]:
	# 			for f in files:
	# 				files_delete(f)
	# 	else:
	# 		# 提取数目异常,跳出
	# 		print(file)
	# 		break
	# # 删除重复urls
	# oldlen = len(urls)
	# urls = list_delsame(urls)
	# newlen = len(urls)
	# if newlen != oldlen:
	# 	print('获取urls数 %d ，去重后得到urls数 %d' % (oldlen,newlen))
	# # 返回结果
	# file_open('./temp/urls',rwb='w',text='\n'.join(totalurls))


def downloadpages():
	paths = []
	d_urls = []
	urls = file_open('./temp/urls',rwb='r').split('\n')
	# 生成url和path列表
	for i in range(len(urls)):
		path = './temp/%06d' % (i+1)
		# 跳过已下载文件
		if not os.path.exists(path):
			d_urls.append(urls[i])
			paths.append(path)
	# 下载所有正文页
	requests_download_muti(d_urls,paths)
		

def xpathpagescontent(xpath_title_STMT,xpath_content_STMT):
	files = files_search('./temp/')
	files.pop() # 剔除‘./temp//urls’文件
	if not os.path.exists('./temp/text'):
		file_open('./temp/text',rwb='w',text='') # 创建结果保存文件
	if not os.path.exists('./temp/used/'):       # 目录是否存在
		os.makedirs('./temp/used/')          				 # 创建目录
	# 从正文页逐页提取正文
	for file in files:
		html = file_open(file,rwb='r')
		titles = html_getxpathlist(html,xpath_title_STMT)
		contents = html_getxpathlist(html,xpath_content_STMT)

		if len(titles)==1 and titles[0]!='' and len(contents)>0:
			text = titles[0]+'\n'+' '.join(contents)
			file_open('./temp/text',rwb='a',text='\n\n'+text)
			files_move(file,'./temp/used')
		else:
			print('正文Xpath提取失败:%s，标题：%s，正文：%s' % (file,titles,contents))
			break






if __name__ == '__main__':
	# urls = creatlistpageurls() 	# 目录页urls（需自行编写）
	# downloadlistpages(urls)		# 下载目录页（无需更改）
	# xpa = '//div[@class="item-info"]/h2[@class="title"]/a/@href' 	# 正文url提取xpath（需自行编写）
	# num = 15 						# 除最后一页外，每页提取到的正文url地址个数（需自行编写）
	xpathpagesurls() 		# 从目录页逐页提取正文页urls（无需更改）
	# downloadpages() 				# 下载所有正文页（无需更改）
	# xpa_title = '//article[contains(@class,"story")]/div[@class="storytitle"]/h1/text()' 	# 正文标题提取xpath（需自行编写）
	# xpa_content = '//article[contains(@class,"story")]/div[@id="storytext"]/p//text()' 	# 正文内容提取xpath（需自行编写） 提取貌似还是有问题，下次注意改成//p试试，不然涉及到<blockquote>里面的<p>获取不到
	# xpathpagescontent(xpa_title,xpa_content) 	# 提取正文内容到text文件（无需更改）
