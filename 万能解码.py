#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import chardet
import html.parser
import urllib.request

'''
Created on 2017-3-25
@author: watfe
@title: 万能解码

decodetest1 			# 字节集解码，比如网页源码用req.content的方式获取到字节集编码，然后解码
decodetest2 			# 如果是文字形式别人复制过来的，只能用下面的遍历解码编码，看看是否正常。
html_unescapeEntities 	# html转义字符解码
urllib.request.unquote 	# url解码
s.encode().decode('')	# 普通的编码解码
'''

def main():
	decodetest1(b'\xe6\x88\x90\xe5\x8a\x9f\xe5\x8a\xa0\xe5\x85\xa5')
	#decodetest2('鉶緎裉鹧榪')
	#html_unescapeEntities("description":"&#x627e;&#x56de;&#x624b;&#x673a;&#xff0c;&#x5b9a;&#x4f4d;&#x5b69;&#x5b50;&#xff0c;")
	#urllib.request.unquote('http%3a%2f%2fwww.baidu.com%2fs%3fwd%3d%E4%BD%A0%E5%A5%BD')  # requests.utils.unquote()
	#r'\u6ee1\u6c49\u5168\u5e2d'.encode().decode('unicode_escape')


def decodetest1(byte):
	aaa='''IBM037|IBM437|IBM500|ASMO-708|DOS-720|ibm737|ibm775|ibm850|ibm852|IBM855|ibm857|IBM00858|IBM860|ibm861|DOS-862|IBM863|IBM864|IBM865|cp866|ibm869|IBM870|windows-874|cp875|shift_jis|ks_c_5601-1987|big5|IBM1026|IBM01047|IBM01140|IBM01141|IBM01142|IBM01143|IBM01144|IBM01145|IBM01146|IBM01147|IBM01148|IBM01149|utf-16|unicodeFFFE|windows-1250|windows-1251|windows-1252|windows-1253|windows-1254|windows-1255|windows-1256|windows-1257|windows-1258|Johab|macintosh|x-mac-japanese|x-mac-chinesetrad|x-mac-korean|x-mac-arabic|x-mac-hebrew|x-mac-greek|x-mac-cyrillic|x-mac-chinesesimp|x-mac-romanian|x-mac-ukrainian|x-mac-thai|x-mac-ce|x-mac-icelandic|x-mac-turkish|x-mac-croatian|utf-32|utf-32BE|x-Chinese_CNS|x-cp20001|x_Chinese-Eten|x-cp20003|x-cp20004|x-cp20005|x-IA5|x-IA5-German|x-IA5-Swedish|x-IA5-Norwegian|us-ascii|x-cp20261|x-cp20269|IBM273|IBM277|IBM278|IBM280|IBM284|IBM285|IBM290|IBM297|IBM420|IBM423|IBM424|x-EBCDIC-KoreanExtended|IBM-Thai|koi8-r|IBM871|IBM880|IBM905|IBM00924|EUC-JP|x-cp20936|x-cp20949|cp1025|koi8-u|iso-8859-1|iso-8859-2|iso-8859-3|iso-8859-4|iso-8859-5|iso-8859-6|iso-8859-7|iso-8859-8|iso-8859-9|iso-8859-13|iso-8859-15|x-Europa|iso-8859-8-i|iso-2022-jp|csISO2022JP|iso-2022-jp|iso-2022-kr|x-cp50227|euc-jp|EUC-CN|euc-kr|hz-gb-2312|gb2312|gbk|GB18030|x-iscii-de|x-iscii-be|x-iscii-ta|x-iscii-te|x-iscii-as|x-iscii-or|x-iscii-ka|x-iscii-ma|x-iscii-gu|x-iscii-pa|utf-7|utf-8'''
	for encoding in aaa.split('|'):
		try:
			sss = byte.decode(encoding)
			print('%15s  %s'% (encoding,sss))
		except:
			pass
	print(chardet.detect(byte))


def decodetest2(text):
	# 中日韩俄常用编码
	code_arr='''shift_jis|koi8-r|euc-kr|euc-jp|ISO-2022-JP|big5|GB18030|utf-8|utf-16|utf-32'''.split('|')
	for c1 in code_arr:
		for c2 in code_arr:
			if c1!=c2:
				try:
					sss = text.encode(c1).decode(c2)
					print('%10s  %10s  %s'% (c1,c2,sss))
				except:
					pass


def html_unescapeEntities(string):
    str_new = string
    str_old = ''
    while str_old != str_new:
        str_old = str_new
        str_new = html.parser.HTMLParser().unescape(str_old)
    print(str_new)



if __name__ == '__main__':
	main()

