#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests,os,codecs,re



'''
Created on 2018-3-27
@author: watfe
@title: 批量重命名

思路说明：
    1、自动检查 文件数 与 新文件名数 是否一致
    2、新文件名 没有扩展名的，自动补上对应文件的扩展名
    3、新文件名中的数字1、2、3前面自动补0，令其长度整齐01、02、03……。
    4、检查新旧文件名中的数字顺序是否正常（是否满足从小到大，逐个+1顺序）
    5、打印出即将替换文件名的对应关系
    6、执行批量重命名

使用方法：修改以下两项然后运行即可
    path：文件所在的目录
    text: 按顺序，作为替换的文件名，一行一个

'''

text = '''第1话 蹭烟的吉恩
第2话 恶友的名字叫尼诺
第3话 飘散在城的烟雾传闻
第4话 封闭之「国」的余烬
第5话 视线前方、重叠的足迹
第6话 轨道与荣誉的尽头
第7话 飘浮在夜雾中的真相
第8话 展翼的王女和朋友的职责
第9话 露出獠牙的优雅黑蛇
第10话 落在浮躁街道的星星
第11话 弗罗旺之花散发恶意之香
第12话 鸟的去向'''

def main():
    # 方法一：对于序号连贯的文件。
    # 将新文件名都放在文本里，重命名所有文件

    path = r'D:\迅雷下载\[2017(12)] ACCA13区监察课 [热血 社会]'
    path = path.replace('\\','/')
    oldnames = GetOldnames(path)                    # 获取目录下要重命名的文件
    #with codecs.open('new.txt','r') as f:text = f.read()    # 从文件中读入新文件名
    newnames = GetNewnames(text)                    # 从文本中读入新文件名
    newnames = ResetNumLen(newnames, '第*话')       # 整理文件名中的数字位数
    newnames = AddExtensions(oldnames,newnames)     # 自动补上扩展名

    CheckListOrder(oldnames,'旧文件名表')            # 检查文件名中的数字顺序
    CheckListOrder(newnames,'新文件名表')            # 检查文件名中的数字顺序
    ShowMapping(oldnames, newnames)                 # 列出一一对应关系

    Renames(path, oldnames, newnames)               # 批量重命名


def main1():
    # 方法二：给定新旧文件名一一对应关系的列表，按列表逐一重命名
    path = 'E:/rrr/'
    aaa = [
    ['1(1797).mp4', '新文件名1.mp4'],
    ['1(1865).mp4', '新文件名2.mp4']]

    for i in aaa:
        try:
            os.rename(path+i[0],path+i[1])
        except:
            print(i[0],'|',i[1])   # 列出重命名失败项


def ResetNumLen(names, pat='第*', length=0): 
    '''补0（整理文件名中的序号位数）
    为了让文件名，排列美观，并且在个系统中能正常排列，需要在数字前面补0。

    Args:
        names: 文件名列表
        pat: 匹配格式
        length: 数字位数（自动取最大长度，也可手动设定）

    Returns:
        arr: 补好0的文件名列表
    '''
    # 初始化
    arr = []
    lstr = pat[:pat.find('*')]      # 数字左边文本
    rstr = pat[pat.find('*')+1:]    # 数字右边文本
    pat  = pat.replace('*',r'(\d+)')
    # 未给定length，检查length最大长度
    if length==0:
        nums = re.findall(pat, '|'.join(names))    # 文件名中的数字
        length = max(list(map(lambda d: len(d),nums)))
    print('     新文件名表：调整[%s*%s]中数字到%d位数'%(lstr,rstr,length))
    # 开始补0
    for i in names:
        d = re.findall(pat, i)    # 文件名中的数字
        if len(d)>1:
            print(' [×] "%s"中发现两个可匹配部分：'%i,d)
            raise ValueError
        else:
            d = d[0]                                    # 文件名中的数字
            nd = ('%0'+str(length)+'d')%int(d)          # 补0后的数字
            nn = i.replace(lstr+d+rstr,lstr+nd+rstr)    # 补0后的文件名
            arr.append(nn)
    return arr


def GetOldnames(path):
    '''返回文件夹中的文件名列表
    
    Args:
        path: 文件夹目录

    returns:
        names: 该文件夹中所有文件
    '''
    names = os.listdir(path)
    print('     原文件名表：共 %d 项'%len(names))
    return names


def GetNewnames(text):
    ''' 将新文件名文本，转换为新文件名列表
    
    去除空行，按行分割文本，整理文件名（去首尾空）

    Args:
        text: 文件名文本

    Returns:
        names: 文件名列表
    '''
    names = text.split('\n')              # 分割文本
    names = [i.strip() for i in names]    # 删首尾空
    while '' in names:                    # 删除''
        names.remove('')
    print('     新文件名表：共 %d 项'%len(names))
    return names


def CheckListOrder(names,title):
    ''' 检查文件名列表中的名称顺序
    
    列表中文件名，取出文件名中的数字（个别文件名可能包含多个数字），
    如果前后两名中，能找到两数字相差为1，说明这两个文件名连贯，否则报错

    例如：
        文件名列表: ['第1话', '第2话', '第3话v2', '第4话']
        取到数字:   [[1],[2],[3,2],[4]]
        能找到前后相差1这样的关系，基本说明连贯

    Args:
        names: 文件名列表
        title: 标签 （原/新文件名列表）
    
    '''
    print('     %s：检查数字顺序中……'%title,end='')
    names = [os.path.splitext(i)[0] for i in names] # 去除扩展名
    for i in range(len(names)):
        if i<len(names)-1:
            r1 = re.findall(r'\d+',names[i])    # 当前文件名中的所有数字
            r2 = re.findall(r'\d+',names[i+1])  # 下一文件名中的所有数字
            if len(r1)>0 and len(r2)>0:
                signal = True                   # 初始化
                for j in r1:                    # 正则可能匹配多个数字
                    for k in r2:                # 正则可能匹配多个数字
                        if int(k)-int(j)==1:    # 前后两文件名中，能有两数字相差为1，说明基本连贯
                            signal=False
                if signal:
                    print(' [!] 警告：文件名可能不连贯。%s 序号%d【%s，%s】'%(title,i,names[i],names[i+1]))
                    raise ValueError
            else:
                print(' [!] 警告：没有找到数字。%s 序号%d【%s，%s】'%(title,i,names[i],names[i+1]))
                raise ValueError
    print('检查完毕')


def ShowMapping(oldnames,newnames):
    ''' 列出对应关系
    
    原文件名和新文件名个数是否相同

        相同：列出对应关系
        不同：报错
    '''
    print('     列出对应关系：')
    if len(oldnames)==len(newnames):
        for i in range(len(oldnames)):
            print('\t\t%s\t→\t%s'%(oldnames[i],newnames[i]))
    else:
        print(' [×] 错误：新文件名%d个，原文件名%d个'%(len(oldnames),len(newnames)))
        raise ValueError
    

def AddExtensions(oldnames,newnames):
    ''' 补齐扩展名
    
    新文件名很可能没有扩展名，需要根据新旧文件名一一对应关系，为新文件名一一补全扩展名
    '''
    print(newnames)
    if len(oldnames) != len(newnames):
        print(' [×] 错误：新文件名%d个，原文件名%d个' % (len(oldnames), len(newnames)))
        raise ValueError
    if '.' in oldnames[0] and '.' not in newnames[0]:    # 没有扩展名的加上扩展名
        names = []
        for i in range(len(oldnames)):
            on = oldnames[i]
            nn = newnames[i]
            nn+=os.path.splitext(on)[1]
            names.append(nn)
        print('     新文件名表：自动补全扩展名')
    else:
        names = newnames
    return names


def Renames(path,oldnames,newnames):
    ''' 批量重命名'''
    print('     开始执行批量重命名')
    for i in range(len(oldnames)):
        op = os.path.join(path,oldnames[i])        # 原文件名完整路径
        np = os.path.join(path,newnames[i])        # 新文件名完整路径
        try:
            os.rename(op,np)
        except:
            print(' [×] 重命名失败：%s\t→\t%s'%(oldnames[i], newnames[i]))
    print(' * * * * * * * * \n执行批量重命名完毕！\n * * * * * * * * ')


if __name__ == '__main__':
    main()
    # main1()
